'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const globbing = require('gulp-sass-glob');


gulp.task('styles', function () {
    return gulp.src('frontend/styles/styles.scss', {base: 'frontend'})
        .pipe(globbing())
        .pipe(sass())
        .pipe(gulp.dest('public'));
});